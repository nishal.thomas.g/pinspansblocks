//
//  PinsPansBlocksProtocol.swift
//  PinsPansBlocks
//
//  Created by Nishal Gnanasundaram on 26/07/21.
//

import Foundation

public protocol PinsPansBlocksProtocol {
    
    static var format3: Format3Protocol { get }
    
}
