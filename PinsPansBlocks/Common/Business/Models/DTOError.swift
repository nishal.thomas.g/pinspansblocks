//
//  DTOError.swift
//  PinsPansBlocks
//
//  Created by Nishal Gnanasundaram on 26/07/21.
//

import Foundation

public struct DTOError: Error {
    
    let type: ErrorType
    let message: String
    
    public init(type: ErrorType, message: String) {
        self.type = type
        self.message = message
    }
    
}

extension DTOError: Equatable {
    
    public static func == (lhs: DTOError, rhs: DTOError) -> Bool {
        return lhs.type == rhs.type
            && lhs.message == rhs.message
    }
    
}

public enum ErrorType {
    
    case pinNotPrepared
    case panNotPrepared
    
}
