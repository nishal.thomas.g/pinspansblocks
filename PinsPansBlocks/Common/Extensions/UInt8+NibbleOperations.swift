//
//  UInt8+NibbleOperations.swift
//  PinsPansBlocks
//
//  Created by Nishal Gnanasundaram on 26/07/21.
//

import Foundation

extension UInt8 {
        
    init(highNibble value: UInt8) {
        self = 0xF0 & (value << 4)
    }
    
    init(lowNibble value: UInt8) {
        self = 0x0F & value
    }
    
}
