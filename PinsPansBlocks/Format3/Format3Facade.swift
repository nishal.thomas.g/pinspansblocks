//
//  Format3Facade.swift
//  PinsPansBlocks
//
//  Created by Nishal Gnanasundaram on 26/07/21.
//

import Foundation

final class Format3Facade: Format3Protocol {
    
    static let totalBytes = Int(8)
    
    static let minimumPinNibbles = Int(4)
    static let maximumPinNibbles = Int(12)
    static let formatNibble = UInt8(highNibble: 3)
    static let compactedPinFormatAndLengthIndex = 0
    static var compactedPinStartIndex = 1
    static var fillNibbleMinimum = UInt8(0xA)
    static var fillNibbleMaximum = UInt8(0xF)
    
    static var minimumPanNibbles = 12
    static var compactedPanStartIndex = 2
    
    static func lengthNibble(value: Int) -> UInt8 {
        return UInt8(lowNibble: UInt8(value))
    }
    
    func preparePin(with pin: [UInt8], onCompletion: @escaping (Result<[UInt8], DTOError>) -> Void) {
        DispatchQueue.global(qos: .userInteractive).async {
            Format3Interactor().preparePin(with: pin) { result in
               DispatchQueue.main.async {
                   switch result {
                   case .success(let pin):
                       onCompletion(.success(pin))
                   case .failure(let error):
                       onCompletion(.failure(error))
                   }
               }
           }
        }
    }
    
    func preparePan(with pan: [UInt8], onCompletion: @escaping (Result<[UInt8], DTOError>) -> Void) {
        DispatchQueue.global(qos: .userInteractive).async {
            Format3Interactor().preparePan(with: pan) { result in
               DispatchQueue.main.async {
                   switch result {
                   case .success(let pan):
                       onCompletion(.success(pan))
                   case .failure(let error):
                       onCompletion(.failure(error))
                   }
               }
           }
        }
    }
    
    func computePinBlock(with pin: [UInt8], pan: [UInt8], onCompletion: @escaping (Result<[UInt8], DTOError>) -> Void) {
        DispatchQueue.global(qos: .userInteractive).async {
            Format3Interactor().computePinBlock(with: pin, pan: pan) { result in
               DispatchQueue.main.async {
                   switch result {
                   case .success(let pinBlock):
                       onCompletion(.success(pinBlock))
                   case .failure(let error):
                       onCompletion(.failure(error))
                   }
               }
           }
        }
    }
    
}
