//
//  Format3Protocol.swift
//  PinsPansBlocks
//
//  Created by Nishal Gnanasundaram on 26/07/21.
//

import Foundation

// Move to common protocol when supporting additional formats
public protocol Format3Protocol {
    
    static var totalBytes: Int { get }
    
    static var minimumPinNibbles: Int { get }
    static var maximumPinNibbles: Int { get }
    static var formatNibble: UInt8 { get }
    static var compactedPinFormatAndLengthIndex: Int { get }
    static var compactedPinStartIndex: Int { get }
    static var fillNibbleMinimum: UInt8 { get }
    static var fillNibbleMaximum: UInt8 { get }
    
    static var minimumPanNibbles: Int { get }
    static var compactedPanStartIndex: Int { get }
    
    static func lengthNibble(value: Int) -> UInt8
    
    // Inputs to prepare methods are expected as non-compacted, meaning data is stored only in low nibble
    // Outputs from prepare methods are compacted, meaning data is stored in both high and low nibbles
    func preparePin(with pin: [UInt8], onCompletion: @escaping (Result<[UInt8], DTOError>) -> Void)
    func preparePan(with pan: [UInt8], onCompletion: @escaping (Result<[UInt8], DTOError>) -> Void)
    
    // Inputs and outputs to this method should expect to be compacted, meaning data is stored in both high and low nibbles
    func computePinBlock(with pin: [UInt8], pan: [UInt8], onCompletion: @escaping (Result<[UInt8], DTOError>) -> Void)
    
}
