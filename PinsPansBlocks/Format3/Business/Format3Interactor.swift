//
//  Format3Interactor.swift
//  PinsPansBlocks
//
//  Created by Nishal Gnanasundaram on 26/07/21.
//

import Foundation

final class Format3Interactor {
    
    func preparePin(with pin: [UInt8], onCompletion: @escaping (Result<[UInt8], DTOError>) -> Void) {
        var pinLength = pin.count
        if pinLength < Format3Facade.minimumPinNibbles {
            // Bail out with error if PIN as too short
            onCompletion(.failure(DTOError(type: .pinNotPrepared, message: "PIN length \(pinLength) shorter than minimum required length \(Format3Facade.minimumPinNibbles)")))
            return
        } else if pinLength > Format3Facade.maximumPinNibbles {
            // Truncate to maximum PIN length if too long
            pinLength = Format3Facade.maximumPinNibbles
        }
        
        // Create empty compacted PIN
        var compactedPin = [UInt8](repeating: 0x00, count: Format3Facade.totalBytes)
        
        // Set format and length of compacted PIN
        compactedPin[Format3Facade.compactedPinFormatAndLengthIndex] = Format3Facade.formatNibble | Format3Facade.lengthNibble(value: pinLength)
        
        // Copy PIN nibbles into compacted PIN
        for (nibbleIndex, pinNibble) in pin[0..<pinLength].enumerated() {
            let compactedIndex = Format3Facade.compactedPinStartIndex + nibbleIndex / 2
            if nibbleIndex % 2 == 0 {
                compactedPin[compactedIndex] = UInt8(highNibble: pinNibble)
            } else {
                compactedPin[compactedIndex] |= UInt8(lowNibble: pinNibble)
            }
        }
        
        // Fill random values from 10 to 15 into compacted PIN
        for nibbleIndex in (Format3Facade.compactedPinStartIndex * 2 + pinLength)..<Format3Facade.totalBytes * 2 {
            let compactedIndex = nibbleIndex / 2
            if nibbleIndex % 2 == 0 {
                compactedPin[compactedIndex] = UInt8(highNibble: UInt8.random(in: Format3Facade.fillNibbleMinimum...Format3Facade.fillNibbleMaximum))
            } else {
                compactedPin[compactedIndex] |= UInt8(lowNibble: UInt8.random(in: Format3Facade.fillNibbleMinimum...Format3Facade.fillNibbleMaximum))
            }
        }
        
        onCompletion(.success(compactedPin))
    }
    
    func preparePan(with pan: [UInt8], onCompletion: @escaping (Result<[UInt8], DTOError>) -> Void) {
        var panLength = pan.count
        if panLength < Format3Facade.minimumPanNibbles {
            // Bail out with error if PAN as too short
            onCompletion(.failure(DTOError(type: .panNotPrepared, message: "PAN length \(panLength) shorter than minimum required length \(Format3Facade.minimumPanNibbles)")))
            return
        } else if panLength > Format3Facade.minimumPanNibbles {
            // Truncate to minimum PAN length if too long
            panLength = Format3Facade.minimumPanNibbles
        }
        
        // Create empty compacted PAN
        var compactedPan = [UInt8](repeating: 0x00, count: Format3Facade.totalBytes)
        
        // Copy PAN nibbles into compacted PAN
        for (nibbleIndex, panNibble) in pan[pan.count - Format3Facade.minimumPanNibbles..<pan.count - Format3Facade.minimumPanNibbles + panLength].enumerated() {
            let compactedIndex = Format3Facade.compactedPanStartIndex + nibbleIndex / 2
            if nibbleIndex % 2 == 0 {
                compactedPan[compactedIndex] = UInt8(highNibble: panNibble)
            } else {
                compactedPan[compactedIndex] |= UInt8(lowNibble: panNibble)
            }
        }
        
        onCompletion(.success(compactedPan))
    }
    
    func computePinBlock(with pin: [UInt8], pan: [UInt8], onCompletion: @escaping (Result<[UInt8], DTOError>) -> Void) {
        // Bail out with error if PIN length is not exact
        guard pin.count == Format3Facade.totalBytes else {
            return onCompletion(.failure(DTOError(type: .pinNotPrepared, message: "PIN length \(pin.count) invalid when required length \(Format3Facade.totalBytes)")))
        }
        
        // Bail out with error if PIN format is not 3
        let pinFormat: UInt8 = pin[0] >> 4
        guard pinFormat == Format3Facade.formatNibble >> 4 else {
            return onCompletion(.failure(DTOError(type: .pinNotPrepared, message: "PIN format \(pinFormat) invalid when required format \(Format3Facade.formatNibble >> 4)")))
        }
        
        // Bail out with error if PAN length is not exact
        guard pan.count == Format3Facade.totalBytes else {
            return onCompletion(.failure(DTOError(type: .panNotPrepared, message: "PAN length \(pan.count) invalid when required length \(Format3Facade.totalBytes)")))
        }
        
        // Bail out with error if PAN format is not right
        let panFormat: UInt16 = UInt16(pan[0]) << 8 | UInt16(pan[1])
        guard panFormat == 0x0000 else {
            return onCompletion(.failure(DTOError(type: .panNotPrepared, message: "PAN format \(panFormat) invalid when required format \(0x0000)")))
        }
        
        // Create empty compacted PIN-Block
        var compactedPinBlock = [UInt8](repeating: 0x00, count: Format3Facade.totalBytes)
        
        // Compute PIN-Block as PIN XOR PAN
        for index in 0..<Format3Facade.totalBytes {
            compactedPinBlock[index] = pin[index] ^ pan[index]
        }
        
        onCompletion(.success(compactedPinBlock))
    }
    
}
