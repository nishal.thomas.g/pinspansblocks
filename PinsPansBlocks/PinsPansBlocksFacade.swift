//
//  PinsPansBlocksFacade.swift
//  PinsPansBlocks
//
//  Created by Nishal Gnanasundaram on 26/07/21.
//

import Foundation

public final class PinsPansBlocksFacade: PinsPansBlocksProtocol {
    
    public static var format3: Format3Protocol {
        return Format3Facade()
    }
    
}
