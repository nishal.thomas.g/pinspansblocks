//
//  PinsPansBlocksApp.swift
//  PinsPansBlocks
//
//  Created by Nishal Gnanasundaram on 26/07/21.
//

import SwiftUI

@main
struct PinsPansBlocksApp: App {
    var body: some Scene {
        WindowGroup {
            let store = ContentStore()
            let presenter = ContentPresenter(delegate: store)
            ContentView(presenter: presenter, store: store)
        }
    }
}
