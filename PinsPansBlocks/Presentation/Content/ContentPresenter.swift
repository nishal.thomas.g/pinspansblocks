//
//  ContentPresenter.swift
//  PinsPansBlocks
//
//  Created by Nishal Gnanasundaram on 27/07/21.
//

import Foundation

protocol ContentPresenterProtocol: AnyObject {
    
    func setPin(_ pin: String)
    func computePinBlock()
    
}

protocol ContentPresenterDelegate: AnyObject {
    
    func renderStart()
    func render(errorMessage: String)
    func renderLoading()
    func render(content: ContentViewModel)
    
}

class ContentPresenter: ContentPresenterProtocol {
    
    private weak var delegate: ContentPresenterDelegate?
    private var interactor: Format3Protocol
    
    private var viewContent: ContentViewModel = ContentViewModel(pin: "", pan: ContentPresenter.pan, pinBlock: "")
  
    init(delegate: ContentPresenterDelegate?, interactor: Format3Protocol = PinsPansBlocksFacade.format3) {
        self.delegate = delegate
        self.interactor = interactor
        delegate?.renderStart()
    }
    
    func setPin(_ pin: String) {
        viewContent.pin = pin
    }
    
    func computePinBlock() {
        if !viewContent.pin.allSatisfy(\.isHexDigit) {
            delegate?.render(errorMessage: "Invalid PIN characters, please restrict to hexadecimal characters (0-9, a-f, A-F)")
            return
        }
        
        let pin = viewContent.pin.map { UInt8($0.hexDigitValue!) }
        interactor.preparePin(with: pin) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let compactPin):
                let pan = strongSelf.viewContent.pan.map { UInt8($0.hexDigitValue!) }
                strongSelf.interactor.preparePan(with: pan) { [weak self] result in
                    guard let strongSelf = self else { return }
                    switch result {
                    case .success(let compactPan):
                        strongSelf.interactor.computePinBlock(with: compactPin, pan: compactPan) { [weak self] result in
                            guard let strongSelf = self else { return }
                            switch result {
                            case .success(let pinBlock):
                                strongSelf.viewContent = strongSelf.mapToViewContent(pin: strongSelf.viewContent.pin, pan: strongSelf.viewContent.pan, pinBlock: pinBlock)
                                if strongSelf.viewContent.pinBlock.isEmpty {
                                    strongSelf.delegate?.render(errorMessage: "Unable to convert PIN-Block to display on screen")
                                } else {
                                    strongSelf.delegate?.render(content: strongSelf.viewContent)
                                }
                            case .failure(let error):
                                strongSelf.delegate?.render(errorMessage: error.message)
                            }
                        }
                    case .failure(let error):
                        strongSelf.delegate?.render(errorMessage: error.message)
                    }
                }
            case .failure(let error):
                strongSelf.delegate?.render(errorMessage: error.message)
            }
        }
    }

    private func mapToViewContent(pin: String, pan: String, pinBlock: [UInt8]) -> ContentViewModel {
        return ContentViewModel(pin: pin, pan: pan, pinBlock: pinBlock.compactMap({ String(format: "%02x", $0).uppercased() }).joined(separator: ""))
    }
    
    static let pan = "1111222233334444"
    
}
