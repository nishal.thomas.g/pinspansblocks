//
//  ContentView.swift
//  PinsPansBlocks
//
//  Created by Nishal Gnanasundaram on 26/07/21.
//

import SwiftUI

struct ContentView: View {
    
    var presenter: ContentPresenterProtocol
    @ObservedObject var store: ContentStore
    
    init(presenter: ContentPresenterProtocol, store: ContentStore) {
        self.presenter = presenter
        self.store = store
    }
    
    @State private var pin: String = ""
    
    var body: some View {
        VStack {
            HStack {
                Text("PIN: ")
                TextField("Enter PIN", text: $pin)
            }.padding()
            Text("PAN: \(ContentPresenter.pan)")
                .padding()
            
            switch store.state {
            case .start:
                Text("")
                    .padding()
            case .loading:
                Text("Computing PIN-Block...")
                    .padding()
            case .error(let message):
                Text(message)
                    .padding()
            case .loaded(let content):
                Text("PIN-Block: \(content.pinBlock)")
                    .padding()
            }
            
            Button("Compute PIN-Block") {
                presenter.setPin(pin)
                presenter.computePinBlock()
            }.padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let store = ContentStore()
        let presenter = ContentPresenter(delegate: store)
        ContentView(presenter: presenter, store: store)
    }
}
