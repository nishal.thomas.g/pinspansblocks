//
//  ContentViewModel.swift
//  PinsPansBlocks
//
//  Created by Nishal Gnanasundaram on 27/07/21.
//

import Foundation

struct ContentViewModel {
    
    var pin: String
    var pan: String
    var pinBlock: String
    
}
