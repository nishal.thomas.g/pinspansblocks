//
//  ContentStore.swift
//  PinsPansBlocks
//
//  Created by Nishal Gnanasundaram on 26/07/21.
//

import Foundation

final class ContentStore: ObservableObject {
    
    enum State {
        
        case start
        case loading
        case error(message: String)
        case loaded(content: ContentViewModel)
        
    }
    
    @Published var state: State = .start
    
}

extension ContentStore: ContentPresenterDelegate {
    
    func renderStart() {
        self.state = .start
    }
    
    func render(errorMessage: String) {
        self.state = .error(message: errorMessage)
    }
    
    func renderLoading() {
        self.state = .loading
    }
    
    func render(content: ContentViewModel) {
        self.state = .loaded(content: content)
    }
    
}
