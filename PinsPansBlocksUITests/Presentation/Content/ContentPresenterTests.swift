//
//  ContentPresenterTests.swift
//  PinsPansBlocksUITests
//
//  Created by Nishal Gnanasundaram on 27/07/21.
//

import XCTest

class ContentPresenterTests: XCTestCase {
    
    class MockContentPresenterDelegate: ContentPresenterDelegate {
        
        var renderStartCalled = false
        func renderStart() {
            renderStartCalled = true
        }
        
        var renderErrorMessageCalled = false
        var errorMessage: String?
        func render(errorMessage: String) {
            renderErrorMessageCalled = true
            self.errorMessage = errorMessage
        }
        
        var renderLoadingCalled = false
        func renderLoading() {
            renderLoadingCalled = true
        }
        
        var renderContentCalled = false
        var content: ContentViewModel?
        func render(content: ContentViewModel) {
            renderContentCalled = true
            self.content = content
        }
        
    }
    
    func testInvalidPinRendersInvalidPinError() throws {
        // Arrange
        let e = getExpectation()
        let mockPresenterDelegate = MockContentPresenterDelegate()
        let presenter = ContentPresenter(delegate: mockPresenterDelegate)
        let expectedErrorMessage = "Invalid PIN characters, please restrict to hexadecimal characters (0-9, a-f, A-F)"
        
        // Act
        presenter.setPin("invalid_pin")
        presenter.computePinBlock()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            e.fulfill()
        }
        
        // Assert
        waitForExpectation()
        XCTAssertTrue(mockPresenterDelegate.renderStartCalled)
        XCTAssertTrue(mockPresenterDelegate.renderErrorMessageCalled)
        XCTAssertEqual(mockPresenterDelegate.errorMessage, expectedErrorMessage)
    }
    
    func testPinTooShortRendersPinTooShortError() throws {
        // Arrange
        let e = getExpectation()
        let mockPresenterDelegate = MockContentPresenterDelegate()
        let presenter = ContentPresenter(delegate: mockPresenterDelegate)
        let expectedErrorMessage = "PIN length 2 shorter than minimum required length 4"
        
        // Act
        presenter.setPin("12")
        presenter.computePinBlock()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            e.fulfill()
        }
        
        // Assert
        waitForExpectation()
        XCTAssertTrue(mockPresenterDelegate.renderStartCalled)
        XCTAssertTrue(mockPresenterDelegate.renderErrorMessageCalled)
        XCTAssertEqual(mockPresenterDelegate.errorMessage, expectedErrorMessage)
    }
    
    func testPinMinimumLengthRendersExpectedContentPinBlock() throws {
        // Arrange
        let e = getExpectation()
        let mockPresenterDelegate = MockContentPresenterDelegate()
        let presenter = ContentPresenter(delegate: mockPresenterDelegate)
        let expectedPart = "341216"
        
        // Act
        presenter.setPin("1234")
        presenter.computePinBlock()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            e.fulfill()
        }
        
        // Assert
        waitForExpectation()
        XCTAssertTrue(mockPresenterDelegate.renderStartCalled)
        XCTAssertNil(mockPresenterDelegate.errorMessage)
        XCTAssertTrue(mockPresenterDelegate.renderContentCalled)
        XCTAssertNotNil(mockPresenterDelegate.content)
        XCTAssertTrue(mockPresenterDelegate.content!.pinBlock.starts(with: expectedPart))
    }
    
    func testPinAllowedLengthRendersExpectedContentPinBlock() throws {
        // Arrange
        let e = getExpectation()
        let mockPresenterDelegate = MockContentPresenterDelegate()
        let presenter = ContentPresenter(delegate: mockPresenterDelegate)
        let expectedPart = "3512167"
        
        // Act
        presenter.setPin("12345")
        presenter.computePinBlock()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            e.fulfill()
        }
        
        // Assert
        waitForExpectation()
        XCTAssertTrue(mockPresenterDelegate.renderStartCalled)
        XCTAssertNil(mockPresenterDelegate.errorMessage)
        XCTAssertTrue(mockPresenterDelegate.renderContentCalled)
        XCTAssertNotNil(mockPresenterDelegate.content)
        XCTAssertTrue(mockPresenterDelegate.content!.pinBlock.starts(with: expectedPart))
    }
    
    func testPinMaximumLengthRendersExpectedContentPinBlock() throws {
        // Arrange
        let e = getExpectation()
        let mockPresenterDelegate = MockContentPresenterDelegate()
        let presenter = ContentPresenter(delegate: mockPresenterDelegate)
        let expectedPart = "3C1216744BA9F8"
        
        // Act
        presenter.setPin("123456789ABC")
        presenter.computePinBlock()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            e.fulfill()
        }
        
        // Assert
        waitForExpectation()
        XCTAssertTrue(mockPresenterDelegate.renderStartCalled)
        XCTAssertNil(mockPresenterDelegate.errorMessage)
        XCTAssertTrue(mockPresenterDelegate.renderContentCalled)
        XCTAssertNotNil(mockPresenterDelegate.content)
        XCTAssertTrue(mockPresenterDelegate.content!.pinBlock.starts(with: expectedPart))
    }
    
    func testPinTooLongRendersExpectedContentPinBlock() throws {
        // Arrange
        let e = getExpectation()
        let mockPresenterDelegate = MockContentPresenterDelegate()
        let presenter = ContentPresenter(delegate: mockPresenterDelegate)
        let expectedPart = "3C1216744BA9F8"
        
        // Act
        presenter.setPin("123456789ABCDF0")
        presenter.computePinBlock()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            e.fulfill()
        }
        
        // Assert
        waitForExpectation()
        XCTAssertTrue(mockPresenterDelegate.renderStartCalled)
        XCTAssertNil(mockPresenterDelegate.errorMessage)
        XCTAssertTrue(mockPresenterDelegate.renderContentCalled)
        XCTAssertNotNil(mockPresenterDelegate.content)
        XCTAssertTrue(mockPresenterDelegate.content!.pinBlock.starts(with: expectedPart))
    }

}
