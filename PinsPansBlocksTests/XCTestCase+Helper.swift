//
//  XCTestCase+Helper.swift
//  PinsPansBlocksTests
//
//  Created by Nishal Gnanasundaram on 26/07/21.
//

import XCTest

extension XCTestCase {
    
    func getExpectation() -> XCTestExpectation {
        return expectation(description: "\(#function).\(Date().timeIntervalSince1970)")
    }
    
    func waitForExpectation(timeout: TimeInterval = 10.0) {
        waitForExpectations(timeout: timeout, handler: nil)
    }
    
}
