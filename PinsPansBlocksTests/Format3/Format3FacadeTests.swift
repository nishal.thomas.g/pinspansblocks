//
//  Format3FacadeTests.swift
//  PinsPansBlocksTests
//
//  Created by Nishal Gnanasundaram on 26/07/21.
//

import XCTest

class Format3FacadeTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    // PIN tests
    func testPreparePinReturnsOnMainThread() throws {
        // Arrange
        let e = getExpectation()
        var isMainThread: Bool = false
        
        // Act
        Format3Facade().preparePin(with: []) { _ in
            isMainThread = Thread.isMainThread
            e.fulfill()
        }
        
        // Assert
        waitForExpectation()
        XCTAssertTrue(isMainThread)
    }
    
    func testPreparePinGivenEmptyPinReturnsFailurePinNotPrepared() throws {
        // Arrange
        let e = getExpectation()
        let expected = DTOError(type: .pinNotPrepared, message: "PIN length 0 shorter than minimum required length 4")
        
        // Act
        var actual: DTOError?
        Format3Facade().preparePin(with: []) { result in
            switch result {
            case .failure(let error):
                actual = error
                e.fulfill()
            default:
                XCTFail()
            }
        }
        
        // Assert
        waitForExpectation()
        XCTAssertEqual(actual, expected)
    }
    
    func testPreparePinGivenPinLengthLessThanMinimumReturnsFailurePinNotPrepared() throws {
        // Arrange
        let e = getExpectation()
        let expected = DTOError(type: .pinNotPrepared, message: "PIN length 2 shorter than minimum required length 4")
        
        // Act
        var actual: DTOError?
        Format3Facade().preparePin(with: [1, 2]) { result in
            switch result {
            case .failure(let error):
                actual = error
                e.fulfill()
            default:
                XCTFail()
            }
        }
        
        // Assert
        waitForExpectation()
        XCTAssertEqual(actual, expected)
    }
    
    func testPreparePinGivenMinimumPinLengthReturnsSuccessWithPreparedPin() throws {
        // Arrange
        let e = getExpectation()
        let expectedPart: [UInt8] = [0x34, 0x12, 0x34]
        let expectedRandomStartValue: UInt8 = 0xAA, expectedRandomEndValue: UInt8 = 0xFF
        
        // Act
        var actual: [UInt8]?
        Format3Facade().preparePin(with: [1, 2, 3, 4]) { result in
            switch result {
            case .success(let pinBlock):
                actual = pinBlock
            case .failure(_):
                XCTFail()
            }
            e.fulfill()
        }
        
        // Assert
        waitForExpectation()
        XCTAssertEqual(Array<UInt8>(actual![0..<3]), expectedPart)
        for byte in actual![3..<Format3Facade.totalBytes] {
            XCTAssertGreaterThanOrEqual(byte, expectedRandomStartValue)
            XCTAssertLessThanOrEqual(byte, expectedRandomEndValue)
        }
    }
    
    func testPreparePinGivenPinLengthInAllowedRangeReturnsSuccessWithPreparedPin() throws {
        // Arrange
        let e = getExpectation()
        let expectedPart: [UInt8] = [0x36, 0x12, 0x34, 0x56]
        let expectedRandomStartValue: UInt8 = 0xAA, expectedRandomEndValue: UInt8 = 0xFF
        
        // Act
        var actual: [UInt8]?
        Format3Facade().preparePin(with: [1, 2, 3, 4, 5, 6]) { result in
            switch result {
            case .success(let pinBlock):
                actual = pinBlock
            case .failure(_):
                XCTFail()
            }
            e.fulfill()
        }
        
        // Assert
        waitForExpectation()
        XCTAssertEqual(Array<UInt8>(actual![0..<4]), expectedPart)
        for byte in actual![4..<Format3Facade.totalBytes] {
            XCTAssertGreaterThanOrEqual(byte, expectedRandomStartValue)
            XCTAssertLessThanOrEqual(byte, expectedRandomEndValue)
        }
    }
    
    func testPreparePinGivenMaximumPinLengthReturnsSuccessWithPreparedPin() throws {
        // Arrange
        let e = getExpectation()
        let expectedPart: [UInt8] = [0x3C, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC]
        let expectedRandomStartValue: UInt8 = 0xAA, expectedRandomEndValue: UInt8 = 0xFF
        
        // Act
        var actual: [UInt8]?
        Format3Facade().preparePin(with: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]) { result in
            switch result {
            case .success(let pinBlock):
                actual = pinBlock
            case .failure(_):
                XCTFail()
            }
            e.fulfill()
        }
        
        // Assert
        waitForExpectation()
        XCTAssertEqual(Array<UInt8>(actual![0..<7]), expectedPart)
        for byte in actual![7..<Format3Facade.totalBytes] {
            XCTAssertGreaterThanOrEqual(byte, expectedRandomStartValue)
            XCTAssertLessThanOrEqual(byte, expectedRandomEndValue)
        }
    }
    
    func testPreparePinGivenPinLengthGreaterThanMaximumReturnsSuccessWithPreparedPin() throws {
        // Arrange
        let e = getExpectation()
        let expectedPart: [UInt8] = [0x3C, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0]
        let expectedRandomStartValue: UInt8 = 0xAA, expectedRandomEndValue: UInt8 = 0xFF
        
        // Act
        var actual: [UInt8]?
        Format3Facade().preparePin(with: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]) { result in
            switch result {
            case .success(let pinBlock):
                actual = pinBlock
            case .failure(_):
                XCTFail()
            }
            e.fulfill()
        }
        
        // Assert
        waitForExpectation()
        XCTAssertEqual(actual![0..<7], expectedPart[0..<7])
        for byte in actual![7..<Format3Facade.totalBytes] {
            XCTAssertGreaterThanOrEqual(byte, expectedRandomStartValue)
            XCTAssertLessThanOrEqual(byte, expectedRandomEndValue)
        }
    }
    
    // PAN tests
    func testPreparePanReturnsOnMainThread() throws {
        // Arrange
        let e = getExpectation()
        var isMainThread: Bool = false
        
        // Act
        Format3Facade().preparePan(with: []) { _ in
            isMainThread = Thread.isMainThread
            e.fulfill()
        }
        
        // Assert
        waitForExpectation()
        XCTAssertTrue(isMainThread)
    }
    
    func testPreparePanGivenPanLengthLessThanMinimumReturnsFailurePanNotPrepared() throws {
        // Arrange
        let e = getExpectation()
        let expected = DTOError(type: .panNotPrepared, message: "PAN length 2 shorter than minimum required length 12")
        
        // Act
        var actual: DTOError?
        Format3Facade().preparePan(with: [1, 2]) { result in
            switch result {
            case .failure(let error):
                actual = error
                e.fulfill()
            default:
                XCTFail()
            }
        }
        
        // Assert
        waitForExpectation()
        XCTAssertEqual(actual, expected)
    }
    
    func testPreparePanGivenMinimumPanLengthReturnsSuccessWithPreparedPan() throws {
        // Arrange
        let e = getExpectation()
        let expected: [UInt8] = [0x00, 0x00, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC]
        
        // Act
        var actual: [UInt8]?
        Format3Facade().preparePan(with: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]) { result in
            switch result {
            case .success(let pinBlock):
                actual = pinBlock
            case .failure(_):
                XCTFail()
            }
            e.fulfill()
        }
        
        // Assert
        waitForExpectation()
        XCTAssertEqual(actual, expected)
    }
    
    func testPreparePanGivenPanLengthGreaterThanMinimumReturnsSuccessWithPreparedPan() throws {
        // Arrange
        let e = getExpectation()
        let expected: [UInt8] = [0x00, 0x00, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0]
        
        // Act
        var actual: [UInt8]?
        Format3Facade().preparePan(with: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0]) { result in
            switch result {
            case .success(let pinBlock):
                actual = pinBlock
            case .failure(_):
                XCTFail()
            }
            e.fulfill()
        }
        
        // Assert
        waitForExpectation()
        XCTAssertEqual(actual, expected)
    }
    
    // PIN-Block tests
    func testComputePinBlockReturnsOnMainThread() throws {
        // Arrange
        let e = getExpectation()
        var isMainThread: Bool = false
        
        // Act
        Format3Facade().computePinBlock(with: [], pan: []) { _ in
            isMainThread = Thread.isMainThread
            e.fulfill()
        }
        
        // Assert
        waitForExpectation()
        XCTAssertTrue(isMainThread)
    }
    
    func testComputePinBlockGivenInvalidPinLengthReturnsFailurePinNotPrepared() throws {
        // Arrange
        let e = getExpectation()
        let expected = DTOError(type: .pinNotPrepared, message: "PIN length 2 invalid when required length 8")
        
        // Act
        var actual: DTOError?
        Format3Facade().computePinBlock(with: [1, 2], pan: []) { result in
            switch result {
            case .failure(let error):
                actual = error
                e.fulfill()
            default:
                XCTFail()
            }
        }
        
        // Assert
        waitForExpectation()
        XCTAssertEqual(actual, expected)
    }
    
    func testComputePinBlockGivenInvalidPinFormatReturnsFailurePinNotPrepared() throws {
        // Arrange
        let e = getExpectation()
        let expected = DTOError(type: .pinNotPrepared, message: "PIN format 1 invalid when required format 3")
        
        // Act
        var actual: DTOError?
        Format3Facade().computePinBlock(with: [0x1C, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE], pan: []) { result in
            switch result {
            case .failure(let error):
                actual = error
                e.fulfill()
            default:
                XCTFail()
            }
        }
        
        // Assert
        waitForExpectation()
        XCTAssertEqual(actual, expected)
    }
    
    func testComputePinBlockGivenInvalidPanLengthReturnsFailurePanNotPrepared() throws {
        // Arrange
        let e = getExpectation()
        let expected = DTOError(type: .panNotPrepared, message: "PAN length 2 invalid when required length 8")
        
        // Act
        var actual: DTOError?
        Format3Facade().computePinBlock(with: [0x3C, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE], pan: [0x12, 0x34]) { result in
            switch result {
            case .failure(let error):
                actual = error
                e.fulfill()
            default:
                XCTFail()
            }
        }
        
        // Assert
        waitForExpectation()
        XCTAssertEqual(actual, expected)
    }
    
    func testComputePinBlockGivenInvalidPanFormatReturnsFailurePanNotPrepared() throws {
        // Arrange
        let e = getExpectation()
        let expected = DTOError(type: .panNotPrepared, message: "PAN format 4660 invalid when required format 0")
        
        // Act
        var actual: DTOError?
        Format3Facade().computePinBlock(with: [0x3C, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE], pan: [0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0]) { result in
            switch result {
            case .failure(let error):
                actual = error
                e.fulfill()
            default:
                XCTFail()
            }
        }
        
        // Assert
        waitForExpectation()
        XCTAssertEqual(actual, expected)
    }
    
    func testComputePinBlockGivenValidPinAndPanReturnsSuccessWithComputedPinBlock() throws {
        // Arrange
        let e = getExpectation()
        let expected: [UInt8] = [0x3C, 0x12, 0x26, 0x62, 0x2E, 0xE2, 0x26, 0x62]
        
        // Act
        var actual: [UInt8]?
        Format3Facade().computePinBlock(with: [0x3C, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE], pan: [0x00, 0x00, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC]) { result in
            switch result {
            case .success(let pinBlock):
                actual = pinBlock
            case .failure(_):
                XCTFail()
            }
            e.fulfill()
        }
        
        // Assert
        waitForExpectation()
        XCTAssertEqual(actual, expected)
    }

}
